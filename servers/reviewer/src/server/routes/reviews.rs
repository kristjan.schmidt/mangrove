use crate::database::{DbConn, Query};
use crate::error::Error;
use crate::fetch::get_reviews;
use crate::rdf::IntoRDF;
use crate::server::{Accept, Csv, APPLICATION_N_TRIPLES_CONTENT_TYPE_STR};
use axum::extract::Query as QueryExtractor;
use axum::extract::State;
use axum::http::header::CONTENT_TYPE;
use axum::response::IntoResponse;
use axum::Json;
use csv::Writer;

/// Request reviews matching the provided query.
#[utoipa::path(
get,
tag = "Review",
path = "/reviews",
params(
("q" = Option < String >, Query, nullable = false, description = "Search for reviews that have this string in `sub` or `opinion` field.", example = "restaurants in zurich"),
("signature" = Option < String >, Query, nullable = false, description = "Review with this `signature` value."),
("kid" = Option < String >, Query, nullable = false, description = "Reviews by issuer with the following PEM public key.", example = "-----BEGIN PUBLIC KEY-----MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEDo6mN4kY6YFhpvF0u3hfVWD1RnDElPweX3U3KiUAx0dVeFLPAmeKdQY3J5agY3VspnHo1p/wH9hbZ63qPbCr6g==-----END PUBLIC KEY-----"),
("iat" = Option < i64 >, Query, nullable = false, description = "Reviews issued at this UNIX time."),
("gt_iat" = Option < i64 >, Query, nullable = false, description = "Reviews with UNIX timestamp greater than this."),
("sub" = Option < String >, Query, format = "uri", nullable = false, description = "Reviews of the given subject URI."),
("rating" = Option < i16 >, Query, nullable = false, minimum = 0, maximum = 100, description = "Reviews with the given rating."),
("opinion" = Option < String >, Query, nullable = false, description = "Reviews with the given opinion."),
("limit" = Option < i64 >, Query, nullable = false, description = "Maximum number of reviews that will be returned."),
("opinionated" = Option < bool >, Query, nullable = false, description = "Only reviews which either contain or do not contain an opinion."),
("examples" = Option < bool >, Query, nullable = false, description = "Include reviews left for example subjects `https://example.com` and `geo:0,0?q=*u=*`."),
("issuers" = Option < bool >, Query, nullable = false, description = "Include aggregate information about review issuers."),
("maresi_subjects" = Option < bool >, Query, nullable = false, description = "Include aggregate information about reviews of returned reviews.")
),
responses(
(status = 200, content(("application/json" = Reviews), ("application/n-triples" = String), ("text/csv" = String))),
(status = 400, description = "Provided request has incorrect format.", body = String),
(status = 500, description = "Server encountered an error when processing the request.", body = String)
)
)]
pub async fn get_reviews_route(
    State(db_conn): State<DbConn>,
    accept: Accept,
    QueryExtractor(query): QueryExtractor<Query>,
) -> Result<impl IntoResponse, Error> {
    let accept = accept.0.unwrap_or(vec![mime::APPLICATION_JSON.to_string()]);
    let reviews = get_reviews(db_conn, query).await?;

    // Request with accept header application/json
    if accept.contains(&mime::APPLICATION_JSON.to_string()) {
        Ok(Json(reviews).into_response())

        // Request with accept header application/n-triples
    } else if accept.contains(&APPLICATION_N_TRIPLES_CONTENT_TYPE_STR.to_string()) {
        Ok((
            [(CONTENT_TYPE, APPLICATION_N_TRIPLES_CONTENT_TYPE_STR)],
            reviews.into_ntriples()?,
        )
            .into_response())

        // Suppose it is web browser request
    } else if accept.contains(&mime::TEXT_HTML.to_string()) {
        let mut wtr = Writer::from_writer(vec![]);
        wtr.write_record(&[
            "signature",
            "pem",
            "iat",
            "sub",
            "rating",
            "opinion",
            "images",
            "metadata",
        ])?;
        for review in reviews.reviews {
            let pl = review.payload;
            wtr.write_record(&[
                review.signature,
                review.kid,
                pl.iat.to_string(),
                pl.sub,
                pl.rating.map_or("none".into(), |r| r.to_string()),
                pl.opinion.unwrap_or("none".into()),
                pl.images.map_or("none".into(), |eh| eh.to_string()),
                pl.metadata.map_or("none".into(), |m| m.to_string()),
            ])?;
        }
        Ok(Csv(String::from_utf8(wtr.into_inner()?)?).into_response())

        // Otherwise return application_json format
    } else {
        Ok(Json(reviews).into_response())
    }
}

use crate::aggregator::{Issuer, Statistic};
use crate::database::DbConn;
use crate::error::Error;
use axum::extract::{Path, State};
use axum::response::IntoResponse;
use axum::Json;

/// Request aggregate information about the reviewer.
#[utoipa::path(
get,
tag = "Review",
path = "/issuer/{pem}",
params(
("pem" = String, Path, description = "Request aggregate information about the reviewer.", example = "-----BEGIN PUBLIC KEY-----MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEDo6mN4kY6YFhpvF0u3hfVWD1RnDElPweX3U3KiUAx0dVeFLPAmeKdQY3J5agY3VspnHo1p/wH9hbZ63qPbCr6g==-----END PUBLIC KEY-----"),
),
responses(
(status = 200, description = "Success", content_type = "application/json", body = Issuer),
(status = 400, description = "Provided request has incorrect format.", body = String),
(status = 500, description = "Server encountered an error when processing the request.", body = String)
)
)]
pub async fn get_issuer(
    State(db_conn): State<DbConn>,
    Path(pem): Path<String>,
) -> Result<impl IntoResponse, Error> {
    let pem = pem.replace('+', " ");
    Ok(Json(Issuer::compute(&db_conn, pem).await?).into_response())
}

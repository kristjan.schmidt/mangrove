use headers::ContentType;
use lazy_static::lazy_static;
use std::str::FromStr;

pub const APPLICATION_N_TRIPLES_CONTENT_TYPE_STR: &str = "application/n-triples";

lazy_static! {
    pub static ref APPLICATION_N_TRIPLES_CONTENT_TYPE: ContentType =
        ContentType::from_str(APPLICATION_N_TRIPLES_CONTENT_TYPE_STR).unwrap();
}

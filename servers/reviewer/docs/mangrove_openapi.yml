openapi: 3.0.3
info:
  title: Mangrove Server API
  description: |-
    Submit and retrieve reviews,
        as well as aggregate statistics about reviews.
        More information about the values and types can be found in the
        [Open Reviews Standard](https://mangrove.reviews/standard).
        For more description and example usage see [Mangrove Demo UI](https://mangrove.reviews)
        and [Mangrove Client JS Library](https://js.mangrove.reviews/)
        for a JavaScript wrapper of this API.
  contact:
    name: Open Reviews Association
    email: hello@open-reviews.net
  license:
    name: ''
  version: 0.2.0
servers:
- url: https://api.mangrove.reviews
paths:
  /:
    get:
      tags:
      - Common
      operationId: index
      responses:
        '200':
          description: Mangrove api index page
          content:
            text/plain:
              schema:
                type: string
        '500':
          description: Server encountered an error when processing the request.
          content:
            text/plain:
              schema:
                type: string
  /batch:
    post:
      tags:
      - Review
      summary: Retrieve aggregates for multiple subjects or issuers.
      description: Retrieve aggregates for multiple subjects or issuers.
      operationId: batch
      requestBody:
        description: ''
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/BatchQuery'
        required: true
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BatchReturn'
        '400':
          description: Provided request has incorrect format.
          content:
            text/plain:
              schema:
                type: string
        '500':
          description: Server encountered an error when processing the request.
          content:
            text/plain:
              schema:
                type: string
  /geo:
    get:
      tags:
      - Review
      summary: Request reviews within provided coordinates.
      description: Request reviews within provided coordinates.
      operationId: get_reviews_by_coordinates
      parameters:
      - name: xmin
        in: query
        description: Minimum longitude
        required: true
        schema:
          type: number
          format: float
      - name: ymin
        in: query
        description: Minimum latitude
        required: true
        schema:
          type: number
          format: float
      - name: xmax
        in: query
        description: Maximum longitude
        required: true
        schema:
          type: number
          format: float
      - name: ymax
        in: query
        description: Maximum latitude
        required: true
        schema:
          type: number
          format: float
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Reviews'
            application/n-triples:
              schema:
                type: string
        '400':
          description: Provided request has incorrect format.
          content:
            text/plain:
              schema:
                type: string
        '500':
          description: Server encountered an error when processing the request.
          content:
            text/plain:
              schema:
                type: string
  /issuer/{pem}:
    get:
      tags:
      - Review
      summary: Request aggregate information about the reviewer.
      description: Request aggregate information about the reviewer.
      operationId: get_issuer
      parameters:
      - name: pem
        in: path
        description: Request aggregate information about the reviewer.
        required: true
        schema:
          type: string
        example: '-----BEGIN PUBLIC KEY-----MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEDo6mN4kY6YFhpvF0u3hfVWD1RnDElPweX3U3KiUAx0dVeFLPAmeKdQY3J5agY3VspnHo1p/wH9hbZ63qPbCr6g==-----END PUBLIC KEY-----'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Issuer'
        '400':
          description: Provided request has incorrect format.
          content:
            text/plain:
              schema:
                type: string
        '500':
          description: Server encountered an error when processing the request.
          content:
            text/plain:
              schema:
                type: string
  /review/{signature}:
    get:
      tags:
      - Review
      summary: Request review with the specified signature.
      description: Request review with the specified signature.
      operationId: get_review_json
      parameters:
      - name: signature
        in: path
        description: Signature of the review being requested.
        required: true
        schema:
          type: string
        example: PDSpnKtHioXykdBCMA15y5cLuYrRbSexscvySt_ryjppDWaW1I1AijjWercZE6K-cbS18bCwSmgIPqRIuL-cow
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Review'
            application/n-triples:
              schema:
                type: string
        '400':
          description: Provided request has incorrect format.
          content:
            text/plain:
              schema:
                type: string
        '404':
          description: Signature not found
          content:
            text/plain:
              schema:
                type: string
        '500':
          description: Server encountered an error when processing the request.
          content:
            text/plain:
              schema:
                type: string
  /reviews:
    get:
      tags:
      - Review
      summary: Request reviews matching the provided query.
      description: Request reviews matching the provided query.
      operationId: get_reviews_route
      parameters:
      - name: q
        in: query
        description: Search for reviews that have this string in `sub` or `opinion` field.
        required: false
        schema:
          type: string
        example: restaurants in zurich
      - name: signature
        in: query
        description: Review with this `signature` value.
        required: false
        schema:
          type: string
      - name: kid
        in: query
        description: Reviews by issuer with the following PEM public key.
        required: false
        schema:
          type: string
        example: '-----BEGIN PUBLIC KEY-----MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEDo6mN4kY6YFhpvF0u3hfVWD1RnDElPweX3U3KiUAx0dVeFLPAmeKdQY3J5agY3VspnHo1p/wH9hbZ63qPbCr6g==-----END PUBLIC KEY-----'
      - name: iat
        in: query
        description: Reviews issued at this UNIX time.
        required: false
        schema:
          type: integer
          format: int64
      - name: gt_iat
        in: query
        description: Reviews with UNIX timestamp greater than this.
        required: false
        schema:
          type: integer
          format: int64
      - name: sub
        in: query
        description: Reviews of the given subject URI.
        required: false
        schema:
          type: string
          format: uri
      - name: rating
        in: query
        description: Reviews with the given rating.
        required: false
        schema:
          type: integer
          format: int16
          maximum: 100
          minimum: 0
      - name: opinion
        in: query
        description: Reviews with the given opinion.
        required: false
        schema:
          type: string
      - name: limit
        in: query
        description: Maximum number of reviews that will be returned.
        required: false
        schema:
          type: integer
          format: int64
      - name: opinionated
        in: query
        description: Only reviews which either contain or do not contain an opinion.
        required: false
        schema:
          type: boolean
      - name: examples
        in: query
        description: Include reviews left for example subjects `https://example.com` and `geo:0,0?q=*u=*`.
        required: false
        schema:
          type: boolean
      - name: issuers
        in: query
        description: Include aggregate information about review issuers.
        required: false
        schema:
          type: boolean
      - name: maresi_subjects
        in: query
        description: Include aggregate information about reviews of returned reviews.
        required: false
        schema:
          type: boolean
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Reviews'
            application/n-triples:
              schema:
                type: string
            text/csv:
              schema:
                type: string
        '400':
          description: Provided request has incorrect format.
          content:
            text/plain:
              schema:
                type: string
        '500':
          description: Server encountered an error when processing the request.
          content:
            text/plain:
              schema:
                type: string
  /subject/{sub}:
    get:
      tags:
      - Review
      summary: Request aggregate information about the subject.
      description: Request aggregate information about the subject.
      operationId: get_subject
      parameters:
      - name: sub
        in: path
        description: Request aggregate information about the subject.
        required: true
        schema:
          type: string
        example: https://nytimes.com
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Subject'
        '400':
          description: Provided request has incorrect format.
          content:
            text/plain:
              schema:
                type: string
        '500':
          description: Server encountered an error when processing the request.
          content:
            text/plain:
              schema:
                type: string
  /submit/{jwt_review}:
    put:
      tags:
      - Review
      summary: Submit a new review.
      description: Submit a new review.
      operationId: submit_review_jwt
      parameters:
      - name: jwt_review
        in: path
        description: Mangrove Review in JSON Web Token format as described in the [Open Reviews Standard](https://mangrove.reviews/standard). Please use `https://example.com` or `geo:0,0?q=*&u=*` in subject `sub` field for test reviews which should be removed later.
        required: true
        schema:
          type: string
        example: eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ii0tLS0tQkVHSU4gUFVCTElDIEtFWS0tLS0tTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFcDc4Zms1eUNqYmlZYXZ5UjZGQ2xxcTlBRkJUaXpBSG1ZdU9rcTR3cy9aYmdleG41SVQ2bi83NGt2YlZ0UGxNc3A5Z2luTysxMVZ4ZUorbVFJQ1pZamc9PS0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLSIsImp3ayI6IntcImNydlwiOlwiUC0yNTZcIixcImV4dFwiOnRydWUsXCJrZXlfb3BzXCI6W1widmVyaWZ5XCJdLFwia3R5XCI6XCJFQ1wiLFwieFwiOlwicDc4Zms1eUNqYmlZYXZ5UjZGQ2xxcTlBRkJUaXpBSG1ZdU9rcTR3c19aWVwiLFwieVwiOlwiNEhzWi1TRS1wXy0tSkwyMWJUNVRMS2ZZSXB6dnRkVmNYaWZwa0NBbVdJNFwifSJ9.eyJpYXQiOjE1ODA5MTAwMjIsInN1YiI6Imh0dHBzOi8vbWFuZ3JvdmUucmV2aWV3cyIsInJhdGluZyI6NzUsIm9waW5pb24iOiJHcmVhdCB3ZWJzaXRlIGZvciByZXZpZXdzLiIsIm1ldGFkYXRhIjp7Im5pY2tuYW1lIjoiam9objEyMyIsImNsaWVudF9pZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAwMCJ9fQ.7xQtIlHuDdCVioyztj8i3zJ8dk3oCSfKr6VCR5RtBn6sBcqvpfyvs13PlKGJoamKzx8xUgQTQJjRPv5s91-VLQ
      responses:
        '200':
          description: Review is in the database.
          content:
            text/plain:
              schema:
                type: boolean
        '400':
          description: Provided request has incorrect format.
          content:
            text/plain:
              schema:
                type: string
        '500':
          description: Server encountered an error when processing the request.
          content:
            text/plain:
              schema:
                type: string
components:
  schemas:
    BatchQuery:
      type: object
      description: Query allowing for retrieval of information about multiple subjects or issuers.
      required:
      - pems
      - subs
      properties:
        pems:
          type: array
          items:
            type: string
          description: List of issuer public keys to get information about.
          nullable: true
        subs:
          type: array
          items:
            type: string
            format: uri
          description: A map from Review identifiers (`urn:maresi:<signature>`) to information about the reviews of that review.
          nullable: true
    BatchReturn:
      type: object
      required:
      - issuers
      - subjects
      properties:
        issuers:
          type: object
          additionalProperties:
            $ref: '#/components/schemas/Issuer'
          nullable: true
        subjects:
          type: object
          additionalProperties:
            $ref: '#/components/schemas/Subject'
          nullable: true
    Issuer:
      type: object
      description: Information about a review issuer.
      required:
      - count
      properties:
        count:
          type: integer
          description: Number of reviews written by this issuer.
          minimum: 0
        neutrality:
          type: number
          format: float
          description: How likely is it for this reviewer to be fair.
          maximum: 1
          minimum: 0
    Payload:
      type: object
      description: Primary content of the review, this is what gets serialized for signing.
      required:
      - iat
      - sub
      - rating
      - opinion
      - images
      - metadata
      properties:
        iat:
          type: integer
          format: int64
          description: Unix Time at which the review was signed.
        images:
          type: array
          items:
            type: object
            properties:
              label:
                type: string
              src:
                type: string
                format: url
          description: Images to be included in the review, contain URLs and optional labels.
          nullable: true
        metadata:
          type: object
          description: Any data relating to the issuer or circumstances of leaving review.
          properties:
            client_id:
              type: string
            nickname:
              type: string
          nullable: true
        opinion:
          type: string
          description: Text of an opinion that the issuer had about the subject.
          nullable: true
          maxLength: 1000
        rating:
          type: integer
          format: int16
          description: Rating indicating how likely the issuer is to recommend the subject.
          nullable: true
          maximum: 100
          minimum: 0
        sub:
          type: string
          format: uri
          description: List of subject URIs to get information about.
    Review:
      type: object
      description: Mangrove Review in encoded JWT form and with decoded fields.
      required:
      - jwt
      - kid
      - payload
      - signature
      properties:
        jwt:
          type: string
          description: Review in JWT format.
        kid:
          type: string
          description: Public key of the reviewer in PEM format.
        payload:
          allOf:
          - $ref: '#/components/schemas/Payload'
          description: Primary content of the review.
        signature:
          type: string
          description: JWT signature by the review issuer.
    Reviews:
      type: object
      description: Return type used to provide `Review`s and any associated data.
      required:
      - issuers
      - maresi_subjects
      - reviews
      properties:
        issuers:
          type: object
          description: A map from public keys to information about issuers.
          additionalProperties:
            $ref: '#/components/schemas/Issuer'
          nullable: true
        maresi_subjects:
          type: object
          description: A map from Review identifiers (`urn:maresi:<signature>`) to information about the reviews of that review.
          additionalProperties:
            $ref: '#/components/schemas/Subject'
          nullable: true
        reviews:
          type: array
          items:
            $ref: '#/components/schemas/Review'
          description: A list of reviews satisfying the query.
    Subject:
      type: object
      description: Information about a subject of reviews.
      required:
      - sub
      - quality
      - count
      - opinion_count
      - positive_count
      - confirmed_count
      properties:
        confirmed_count:
          type: integer
          format: uint
          description: Number of reviews with rating above 50 and `is_personal_experience` flag given to this subject.
          minimum: 0
        count:
          type: integer
          format: uint
          description: Number of reviews given to this subject.
          minimum: 0
        opinion_count:
          type: integer
          format: uint
          description: Number of reviews which included an opinion.
          minimum: 0
        positive_count:
          type: integer
          format: uint
          description: Number of reviews with rating above 50 given to this subject.
          minimum: 0
        quality:
          type: integer
          format: int16
          description: Aggregate number representing quality of the subject.
          nullable: true
        sub:
          type: string
          format: uri
          description: URI uniquely identifying the subject.
    UncertainPoint:
      type: object
      required:
      - coordinates
      properties:
        coordinates:
          type: object
          nullable: true
        uncertainty:
          type: integer
          format: int32
          nullable: true

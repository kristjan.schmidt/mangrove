/*
 * Mangrove Server API
 *
 * Submit and retrieve reviews, as well as aggregate statistics about reviews. More information about the values and types can be found in the [Open Reviews Standard](https://mangrove.reviews/standard). For more description and example usage see [Mangrove Demo UI](https://mangrove.reviews) and [Mangrove Client JS Library](https://js.mangrove.reviews/) for a JavaScript wrapper of this API.
 *
 * The version of the OpenAPI document: 0.1.1
 *
 * Generated by: https://openapi-generator.tech
 */
use serde_derive::{Deserialize, Serialize};
/// Subject : Information about a subject of reviews.

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Subject {
    /// Number of reviews with rating above 50 and `is_personal_experience` flag given to this subject.
    #[serde(rename = "confirmed_count")]
    pub confirmed_count: i32,
    /// Number of reviews given to this subject.
    #[serde(rename = "count")]
    pub count: i32,
    /// Number of reviews which included an opinion.
    #[serde(rename = "opinion_count")]
    pub opinion_count: i32,
    /// Number of reviews with rating above 50 given to this subject.
    #[serde(rename = "positive_count")]
    pub positive_count: i32,
    /// Aggregate number representing quality of the subject.
    #[serde(rename = "quality", deserialize_with = "Option::deserialize")]
    pub quality: Option<i32>,
    /// URI uniquely identifying the subject.
    #[serde(rename = "sub")]
    pub sub: String,
}

impl Subject {
    /// Information about a subject of reviews.
    pub fn new(
        confirmed_count: i32,
        count: i32,
        opinion_count: i32,
        positive_count: i32,
        quality: Option<i32>,
        sub: String,
    ) -> Subject {
        Subject {
            confirmed_count,
            count,
            opinion_count,
            positive_count,
            quality,
            sub,
        }
    }
}

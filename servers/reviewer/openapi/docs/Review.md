# Review

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jwt** | **String** | Review in JWT format. | 
**kid** | **String** | Public key of the reviewer in PEM format. | 
**payload** | [**crate::models::Payload**](Payload.md) | Primary content of the review. | 
**signature** | **String** | JWT signature by the review issuer. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



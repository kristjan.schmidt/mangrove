# \DefaultApi

All URIs are relative to *https://api.mangrove.reviews*

Method | HTTP request | Description
------------- | ------------- | -------------
[**batch_post**](DefaultApi.md#batch_post) | **POST** /batch | 
[**issuer_pem_get**](DefaultApi.md#issuer_pem_get) | **GET** /issuer/{pem} | 
[**review_signature_get**](DefaultApi.md#review_signature_get) | **GET** /review/{signature} | 
[**reviews_get**](DefaultApi.md#reviews_get) | **GET** /reviews | 
[**subject_sub_get**](DefaultApi.md#subject_sub_get) | **GET** /subject/{sub} | 
[**submit_jwt_review_put**](DefaultApi.md#submit_jwt_review_put) | **PUT** /submit/{jwt_review} | 



## batch_post

> crate::models::BatchReturn batch_post(batch_query)


Retrieve aggregates for multiple subjects or issuers.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**batch_query** | [**BatchQuery**](BatchQuery.md) |  | [required] |

### Return type

[**crate::models::BatchReturn**](BatchReturn.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## issuer_pem_get

> crate::models::Issuer issuer_pem_get(pem)


Request aggregate information about the reviewer.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**pem** | **String** |  | [required] |

### Return type

[**crate::models::Issuer**](Issuer.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## review_signature_get

> crate::models::Review review_signature_get(signature)


Request review with the specified signature.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**signature** | **String** | Signature of the review being requested. | [required] |

### Return type

[**crate::models::Review**](Review.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/n-triples, text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## reviews_get

> crate::models::Reviews reviews_get(q, signature, kid, iat, gt_iat, sub, rating, opinion, limit, opinionated, examples, issuers, maresi_subjects)


Request reviews matching the provided query.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**q** | Option<**String**> | Search for reviews that have this string in `sub` or `opinion` field. |  |
**signature** | Option<**String**> | Review with this `signature` value. |  |
**kid** | Option<**String**> | Reviews by issuer with the following PEM public key. |  |
**iat** | Option<**f32**> | Reviews issued at this UNIX time. |  |
**gt_iat** | Option<**f32**> | Reviews with UNIX timestamp greater than this. |  |
**sub** | Option<**String**> | Reviews of the given subject URI. |  |
**rating** | Option<**f32**> | Reviews with the given rating. |  |
**opinion** | Option<**String**> | Reviews with the given opinion. |  |
**limit** | Option<**f32**> | Maximum number of reviews that will be returned. |  |
**opinionated** | Option<**bool**> | Only reviews which either contain or do not contain an opinion. |  |
**examples** | Option<**bool**> | Include reviews left for example subjects `https://example.com` and `geo:0,0?q=*u=*`. |  |
**issuers** | Option<**bool**> | Include aggregate information about review issuers. |  |
**maresi_subjects** | Option<**bool**> | Include aggregate information about reviews of returned reviews. |  |

### Return type

[**crate::models::Reviews**](Reviews.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/n-triples, text/csv, text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## subject_sub_get

> crate::models::Subject subject_sub_get(sub)


Request aggregate information about the subject.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**sub** | **String** | Unique subject identifying URI. | [required] |

### Return type

[**crate::models::Subject**](Subject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## submit_jwt_review_put

> bool submit_jwt_review_put(jwt_review)


Submit a new review.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**jwt_review** | **String** | Mangrove Review in JSON Web Token format as described in the [Open Reviews Standard](https://mangrove.reviews/standard). Please use `https://example.com` or `geo:0,0?q=*&u=*` in subject `sub` field for test reviews which should be removed later. | [required] |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


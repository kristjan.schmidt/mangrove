# BatchQuery

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pems** | Option<**Vec<String>**> | List of issuer public keys to get information about. | 
**subs** | Option<**Vec<String>**> | List of subject URIs to get information about. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



pub use openapi::apis;
pub use openapi::models;

mod test_cases;
use test_cases::run_test_sequence;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Sync + Send>> {
    run_test_sequence().await?;
    Ok(())
}

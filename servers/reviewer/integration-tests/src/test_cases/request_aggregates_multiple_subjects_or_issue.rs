// ############################################################
// Test case Request aggregates for multiple subjects or issue
// ############################################################

use super::{
    TestSequenceResult, CONFIGURATION, TEST_BATCH_QUERY_PEMS, TEST_BATCH_QUERY_PEMS_SUBS,
    TEST_BATCH_QUERY_SUBS,
};
use crate::apis::default_api::batch_post;
use anyhow::{anyhow, Result as AnyhowResult};

// Test 1 Request aggregates by pems and subs
pub async fn test_batch_query_pems_subs_success() -> AnyhowResult<TestSequenceResult> {
    batch_post(&CONFIGURATION, TEST_BATCH_QUERY_PEMS_SUBS.clone())
        .await
        .map(TestSequenceResult::BatchReturn)
        .map_err(|err| anyhow!(err))
}

// Test 2 Request aggregates by only pems
pub async fn test_batch_query_pems_success() -> AnyhowResult<TestSequenceResult> {
    batch_post(&CONFIGURATION, TEST_BATCH_QUERY_PEMS.clone())
        .await
        .map(TestSequenceResult::BatchReturn)
        .map_err(|err| anyhow!(err))
}

// Test 3 Request aggregates by subs
pub async fn test_batch_query_subs_success() -> AnyhowResult<TestSequenceResult> {
    batch_post(&CONFIGURATION, TEST_BATCH_QUERY_SUBS.clone())
        .await
        .map(TestSequenceResult::BatchReturn)
        .map_err(|err| anyhow!(err))
}

// ########################################
// Test case Request Review by coordinates
// ########################################

use super::{TestSequenceResult, CONFIGURATION};
use crate::apis::default_api::get_reviews_by_coordinates;
use anyhow::{anyhow, Result as AnyhowResult};

// Test 1 query third review point inside area
pub async fn test_request_review_by_coordinates_success() -> AnyhowResult<TestSequenceResult> {
    let xmin = -1.0_f32;
    let ymin = -1.0_f32;
    let xmax = 0.15_f32;
    let ymax = 0.15_f32;

    get_reviews_by_coordinates(&CONFIGURATION, xmin, ymin, xmax, ymax)
        .await
        .map(TestSequenceResult::Reviews)
        .map_err(|err| anyhow!(err))
}

// Test 1 query third review point on the boundary
pub async fn test_request_review_by_coordinates_success_boundary(
) -> AnyhowResult<TestSequenceResult> {
    let xmin = 0.0_f32;
    let ymin = -1.0_f32;
    let xmax = 0.15_f32;
    let ymax = 0.15_f32;

    get_reviews_by_coordinates(&CONFIGURATION, xmin, ymin, xmax, ymax)
        .await
        .map(TestSequenceResult::Reviews)
        .map_err(|err| anyhow!(err))
}

// Test 3 query wrong coordinates
// Fail ok
pub async fn test_request_review_by_coordinates_wrong() -> AnyhowResult<TestSequenceResult> {
    let xmin = 1.0_f32;
    let ymin = 1.0_f32;
    let xmax = 2.15_f32;
    let ymax = 2.15_f32;

    get_reviews_by_coordinates(&CONFIGURATION, xmin, ymin, xmax, ymax)
        .await
        .map(TestSequenceResult::Reviews)
        .map_err(|err| anyhow!(err))
}

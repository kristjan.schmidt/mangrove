use crate::apis::configuration::Configuration;
use crate::models::{
    BatchQuery, BatchReturn, Issuer, Payload, PayloadMetadata, Review, Reviews, Subject,
};
use lazy_static::lazy_static;
use std::collections::HashMap;

const BASE_PATH_ENV_VARIABLE: &str = "BASE_PATH";
const BASE_PATH_DEFAULT_VALUE: &str = "http://localhost:8000";

lazy_static! {
    pub static ref CONFIGURATION: Configuration = Configuration {
        base_path: std::env::var(BASE_PATH_ENV_VARIABLE)
            .unwrap_or(BASE_PATH_DEFAULT_VALUE.to_owned()),
        user_agent: None,
        ..Default::default()
    };

    // Private Key
    // -----BEGIN PRIVATE KEY-----
    // MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgy8Y1fH+n//NR0J2P
    // P1I8nQhOGpBRh7yXRpzxHvpH0jWhRANCAAQKS6nGDiuSIAvdRAidkgJRrim5xkoz
    // VPC/V7Vaq9m/QJkGSn2dcGcljaPB+IPEyht1Mrb029t6vBs4UhEVenVd
    // -----END PRIVATE KEY-----
    //
    // JWT Review:
    // eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ii0tLS0tQkVHSU4gUFVCTElDIEtFWS0tLS0tTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFQ2t1cHhnNHJraUFMM1VRSW5aSUNVYTRwdWNaS00xVHd2MWUxV3F2WnYwQ1pCa3A5blhCbkpZMmp3ZmlEeE1vYmRUSzI5TnZiZXJ3Yk9GSVJGWHAxWFE9PS0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLSIsImp3ayI6IntcImNydlwiOlwiUC0yNTZcIixcImV4dFwiOnRydWUsXCJrZXlfb3BzXCI6W1widmVyaWZ5XCJdLFwia3R5XCI6XCJFQ1wiLFwieFwiOlwicDc4Zms1eUNqYmlZYXZ5UjZGQ2xxcTlBRkJUaXpBSG1ZdU9rcTR3c19aWVwiLFwieVwiOlwiNEhzWi1TRS1wXy0tSkwyMWJUNVRMS2ZZSXB6dnRkVmNYaWZwa0NBbVdJNFwifSJ9.eyJpYXQiOjE3MDgwOTg4NDIsInN1YiI6Imh0dHBzOi8vZXhhbXBsZS5vcmciLCJyYXRpbmciOjg1LCJvcGluaW9uIjoiR3JlYXQgdGVzdGluZyBmaXJzdCBsaW5rIGV4YW1wbGUuIiwibWV0YWRhdGEiOnsibmlja25hbWUiOiJUZXN0Tmlja05hbWUxIiwiY2xpZW50X2lkIjoiaHR0cHM6Ly9leGFtcGxlLm9yZyJ9fQ.fWMOYt4feQbeQqpQP1qzOGV3kCExUuj87vIZHPbSV0p7TxuW1xpLfqDFJ3S3WLIVLv_8xP3RsmyuYhbKcFVKYQ
    pub static ref TEST_REVIEW: Review=Review {
            jwt: "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ii0tLS0tQkVHSU4gUFVCTElDIEtFWS0tLS0tTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFQ2t1cHhnNHJraUFMM1VRSW5aSUNVYTRwdWNaS00xVHd2MWUxV3F2WnYwQ1pCa3A5blhCbkpZMmp3ZmlEeE1vYmRUSzI5TnZiZXJ3Yk9GSVJGWHAxWFE9PS0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLSIsImp3ayI6IntcImNydlwiOlwiUC0yNTZcIixcImV4dFwiOnRydWUsXCJrZXlfb3BzXCI6W1widmVyaWZ5XCJdLFwia3R5XCI6XCJFQ1wiLFwieFwiOlwicDc4Zms1eUNqYmlZYXZ5UjZGQ2xxcTlBRkJUaXpBSG1ZdU9rcTR3c19aWVwiLFwieVwiOlwiNEhzWi1TRS1wXy0tSkwyMWJUNVRMS2ZZSXB6dnRkVmNYaWZwa0NBbVdJNFwifSJ9.eyJpYXQiOjE3MDgwOTg4NDIsInN1YiI6Imh0dHBzOi8vZXhhbXBsZS5vcmciLCJyYXRpbmciOjg1LCJvcGluaW9uIjoiR3JlYXQgdGVzdGluZyBmaXJzdCBsaW5rIGV4YW1wbGUuIiwibWV0YWRhdGEiOnsibmlja25hbWUiOiJUZXN0Tmlja05hbWUxIiwiY2xpZW50X2lkIjoiaHR0cHM6Ly9leGFtcGxlLm9yZyJ9fQ.fWMOYt4feQbeQqpQP1qzOGV3kCExUuj87vIZHPbSV0p7TxuW1xpLfqDFJ3S3WLIVLv_8xP3RsmyuYhbKcFVKYQ".to_owned(),
            kid: "-----BEGIN PUBLIC KEY-----MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAECkupxg4rkiAL3UQInZICUa4pucZKM1Twv1e1WqvZv0CZBkp9nXBnJY2jwfiDxMobdTK29NvberwbOFIRFXp1XQ==-----END PUBLIC KEY-----".to_owned(),
            signature: "fWMOYt4feQbeQqpQP1qzOGV3kCExUuj87vIZHPbSV0p7TxuW1xpLfqDFJ3S3WLIVLv_8xP3RsmyuYhbKcFVKYQ".to_owned(),
            payload: Box::new(Payload {
                iat: 1708098842_i64,
                sub: "https://example.org".to_owned(),
                rating: Some(85),
                opinion: Some("Great testing first link example.".to_owned()),
                images: None,
                metadata: Some(Box::new(PayloadMetadata {
                    nickname: Some("TestNickName1".to_owned()),
                    client_id: Some("https://example.org".to_owned()),
                })),
            }),
        };


    // Private Key
    //-----BEGIN PRIVATE KEY-----
    // MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgcNkFtra2LoPJtvip
    // WX/LSO88FvR+28qCV6eNeVv50dmhRANCAAQ1Qccv0ZP+gMSNuFHYpizmhs7G+bSb
    // UbFFACbzLXr2eQ+/4/Pvmy3NPAVa6/eIu/Fd8EXhLUMFyGTgZmU5HQg/
    // -----END PRIVATE KEY-----
    //
    // JWT Review:
    // eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ii0tLS0tQkVHSU4gUFVCTElDIEtFWS0tLS0tTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFTlVISEw5R1Qvb0RFamJoUjJLWXM1b2JPeHZtMG0xR3hSUUFtOHkxNjlua1B2K1B6NzVzdHpUd0ZXdXYzaUx2eFhmQkY0UzFEQmNoazRHWmxPUjBJUHc9PS0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLSIsImp3ayI6IntcImNydlwiOlwiUC0yNTZcIixcImV4dFwiOnRydWUsXCJrZXlfb3BzXCI6W1widmVyaWZ5XCJdLFwia3R5XCI6XCJFQ1wiLFwieFwiOlwicDc4Zms1eUNqYmlZYXZ5UjZGQ2xxcTlBRkJUaXpBSG1ZdU9rcTR3c19aWVwiLFwieVwiOlwiNEhzWi1TRS1wXy0tSkwyMWJUNVRMS2ZZSXB6dnRkVmNYaWZwa0NBbVdJNFwifSJ9.eyJpYXQiOjE3MDc5MDI0OTYsInN1YiI6Imh0dHBzOi8vZXhhbXBsZS5uZXQiLCJyYXRpbmciOjk1LCJvcGluaW9uIjoiR3JlYXQgdGVzdGluZyBzZWNvbmQgbGluayBleGFtcGxlLiIsIm1ldGFkYXRhIjp7Im5pY2tuYW1lIjoiVGVzdE5pY2tOYW1lMiIsImNsaWVudF9pZCI6Imh0dHBzOi8vZXhhbXBsZS5uZXQifX0.KFlA-uVNUVRV9MhD5YsXu_VV0HKGCEnCvb4CUSRMqsP-2ayySSrcQTG6jYMpUKyKeupeY5OpmMZ-n-DRw5I3zg
    pub static ref TEST_REVIEW_SECOND: Review=Review{
            jwt: "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ii0tLS0tQkVHSU4gUFVCTElDIEtFWS0tLS0tTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFTlVISEw5R1Qvb0RFamJoUjJLWXM1b2JPeHZtMG0xR3hSUUFtOHkxNjlua1B2K1B6NzVzdHpUd0ZXdXYzaUx2eFhmQkY0UzFEQmNoazRHWmxPUjBJUHc9PS0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLSIsImp3ayI6IntcImNydlwiOlwiUC0yNTZcIixcImV4dFwiOnRydWUsXCJrZXlfb3BzXCI6W1widmVyaWZ5XCJdLFwia3R5XCI6XCJFQ1wiLFwieFwiOlwicDc4Zms1eUNqYmlZYXZ5UjZGQ2xxcTlBRkJUaXpBSG1ZdU9rcTR3c19aWVwiLFwieVwiOlwiNEhzWi1TRS1wXy0tSkwyMWJUNVRMS2ZZSXB6dnRkVmNYaWZwa0NBbVdJNFwifSJ9.eyJpYXQiOjE3MDc5MDI0OTYsInN1YiI6Imh0dHBzOi8vZXhhbXBsZS5uZXQiLCJyYXRpbmciOjk1LCJvcGluaW9uIjoiR3JlYXQgdGVzdGluZyBzZWNvbmQgbGluayBleGFtcGxlLiIsIm1ldGFkYXRhIjp7Im5pY2tuYW1lIjoiVGVzdE5pY2tOYW1lMiIsImNsaWVudF9pZCI6Imh0dHBzOi8vZXhhbXBsZS5uZXQifX0.KFlA-uVNUVRV9MhD5YsXu_VV0HKGCEnCvb4CUSRMqsP-2ayySSrcQTG6jYMpUKyKeupeY5OpmMZ-n-DRw5I3zg".to_owned(),
            kid: "-----BEGIN PUBLIC KEY-----MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAENUHHL9GT/oDEjbhR2KYs5obOxvm0m1GxRQAm8y169nkPv+Pz75stzTwFWuv3iLvxXfBF4S1DBchk4GZlOR0IPw==-----END PUBLIC KEY-----".to_owned(),
            signature: "KFlA-uVNUVRV9MhD5YsXu_VV0HKGCEnCvb4CUSRMqsP-2ayySSrcQTG6jYMpUKyKeupeY5OpmMZ-n-DRw5I3zg".to_owned(),
            payload: Box::new(Payload {
                iat: 1707902496_i64,
                sub: "https://example.net".to_owned(),
                rating: Some(95),
                opinion: Some("Great testing second link example.".to_owned()),
                images: None,
                metadata: Some(Box::new(PayloadMetadata {
                    nickname: Some("TestNickName2".to_owned()),
                    client_id: Some("https://example.net".to_owned()),
                })),
            }),
    };

    // Private Key
    // -----BEGIN PRIVATE KEY-----
    // MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgYYclYbWRgwrsOtK0
    // q/QaqUWd1nZSij9Hf3bou917FdqhRANCAARyAg1UB1THClKbyzLveBWPV/E16s+/
    // 04qRdiD75qIkD/zQvwxSYUvTLPZlqHKoa6irChcAvq46DTp+ckM0ICU6
    // -----END PRIVATE KEY-----
    // JWT Review:
    // eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ii0tLS0tQkVHSU4gUFVCTElDIEtFWS0tLS0tTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFY2dJTlZBZFV4d3BTbThzeTczZ1ZqMWZ4TmVyUHY5T0trWFlnKythaUpBLzgwTDhNVW1GTDB5ejJaYWh5cUd1b3F3b1hBTDZ1T2cwNmZuSkROQ0FsT2c9PS0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLSIsImp3ayI6IntcImNydlwiOlwiUC0yNTZcIixcImV4dFwiOnRydWUsXCJrZXlfb3BzXCI6W1widmVyaWZ5XCJdLFwia3R5XCI6XCJFQ1wiLFwieFwiOlwicDc4Zms1eUNqYmlZYXZ5UjZGQ2xxcTlBRkJUaXpBSG1ZdU9rcTR3c19aWVwiLFwieVwiOlwiNEhzWi1TRS1wXy0tSkwyMWJUNVRMS2ZZSXB6dnRkVmNYaWZwa0NBbVdJNFwifSJ9.eyJpYXQiOjE3MDkxMzcxMDcsInN1YiI6ImdlbzowLDA_cT1UZXN0TmFtZSZ1PTMwIiwicmF0aW5nIjo5OSwib3BpbmlvbiI6IkdyZWF0IHRlc3RpbmcgZ2VvIGV4YW1wbGUuIiwibWV0YWRhdGEiOnsibmlja25hbWUiOiJUZXN0Tmlja05hbWUzIn19.EEvdlIT-EQAOtVRKC34tirIc9OMQ9vHpVuQKJYW8Vsd1UdFlBHAIG80LPAd4HxBNk4eEkhnsjR6jSzWzQBuNVA
    pub static ref TEST_REVIEW_THIRD: Review=Review{
            jwt: "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ii0tLS0tQkVHSU4gUFVCTElDIEtFWS0tLS0tTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFY2dJTlZBZFV4d3BTbThzeTczZ1ZqMWZ4TmVyUHY5T0trWFlnKythaUpBLzgwTDhNVW1GTDB5ejJaYWh5cUd1b3F3b1hBTDZ1T2cwNmZuSkROQ0FsT2c9PS0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLSIsImp3ayI6IntcImNydlwiOlwiUC0yNTZcIixcImV4dFwiOnRydWUsXCJrZXlfb3BzXCI6W1widmVyaWZ5XCJdLFwia3R5XCI6XCJFQ1wiLFwieFwiOlwicDc4Zms1eUNqYmlZYXZ5UjZGQ2xxcTlBRkJUaXpBSG1ZdU9rcTR3c19aWVwiLFwieVwiOlwiNEhzWi1TRS1wXy0tSkwyMWJUNVRMS2ZZSXB6dnRkVmNYaWZwa0NBbVdJNFwifSJ9.eyJpYXQiOjE3MDkxMzcxMDcsInN1YiI6ImdlbzowLDA_cT1UZXN0TmFtZSZ1PTMwIiwicmF0aW5nIjo5OSwib3BpbmlvbiI6IkdyZWF0IHRlc3RpbmcgZ2VvIGV4YW1wbGUuIiwibWV0YWRhdGEiOnsibmlja25hbWUiOiJUZXN0Tmlja05hbWUzIn19.EEvdlIT-EQAOtVRKC34tirIc9OMQ9vHpVuQKJYW8Vsd1UdFlBHAIG80LPAd4HxBNk4eEkhnsjR6jSzWzQBuNVA".to_owned(),
            kid: "-----BEGIN PUBLIC KEY-----MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEcgINVAdUxwpSm8sy73gVj1fxNerPv9OKkXYg++aiJA/80L8MUmFL0yz2ZahyqGuoqwoXAL6uOg06fnJDNCAlOg==-----END PUBLIC KEY-----".to_owned(),
            signature: "EEvdlIT-EQAOtVRKC34tirIc9OMQ9vHpVuQKJYW8Vsd1UdFlBHAIG80LPAd4HxBNk4eEkhnsjR6jSzWzQBuNVA".to_owned(),
            payload: Box::new(Payload {
                iat: 1709137107_i64,
                sub: "geo:0,0?q=TestName&u=30".to_owned(),
                rating: Some(99),
                opinion: Some("Great testing geo example.".to_owned()),
                images: None,
                metadata: Some(Box::new(PayloadMetadata {
                    nickname: Some("TestNickName3".to_owned()),
                    client_id:None,
                })),
            }),
    };

    pub static ref SUB_TEST_PARAMETER_Q: String="geo:32.123456,0?q=Test&u=30".to_string();

    pub static ref REVIEWS_THIRD_REVIEW: Reviews = Reviews {
        reviews: vec![TEST_REVIEW_THIRD.clone()],
        issuers: None,
        maresi_subjects: None,
    };

    pub static ref REVIEWS_EMPTY: Reviews=Reviews{
        reviews:vec![],
        issuers:None,
        maresi_subjects:None,
    };

    pub static ref TEST_REVIEWS: Reviews = Reviews {
        reviews: vec![TEST_REVIEW.clone()],
        issuers: None,
        maresi_subjects: None,
    };

    pub static ref TEST_SUBJECT_EXAMPLE_ORG: Subject = Subject{
        confirmed_count: 0_i32,
        count: 1_i32,
        opinion_count: 1_i32,
        positive_count: 1_i32,
        quality: None,
        sub: "https://example.org".to_owned(),
    };

    pub static ref TEST_ISSUER_EXAMPLE_ORG: Issuer = Issuer{
        count: 1_i32,
        neutrality: None,
    };

    pub static ref TEST_ISSUER_NOT_FOUND: Issuer = Issuer{
        count: 0_i32,
        neutrality: None,
    };

    pub static ref TEST_BATCH_QUERY_PEMS_SUBS: BatchQuery = BatchQuery{
        pems: Some(vec![TEST_REVIEW.kid.clone(),TEST_REVIEW_SECOND.kid.clone()]),
        subs: Some(vec![TEST_REVIEW.payload.sub.clone(),TEST_REVIEW_SECOND.payload.sub.clone()]),
    };

    pub static ref TEST_BATCH_QUERY_PEMS: BatchQuery = BatchQuery{
        pems: Some(vec![TEST_REVIEW.kid.clone(),TEST_REVIEW_SECOND.kid.clone()]),
        subs: None,
    };

    pub static ref TEST_BATCH_QUERY_SUBS: BatchQuery = BatchQuery{
        pems: None,
        subs: Some(vec![TEST_REVIEW.payload.sub.clone(),TEST_REVIEW_SECOND.payload.sub.clone()]),
    };

    pub static ref TEST_BATCH_RETURN_PEMS_SUBS: BatchReturn = BatchReturn{
        issuers: Some(HashMap::from([(TEST_REVIEW_SECOND.kid.clone(), Issuer{
            count: 1_i32,
            neutrality: None
        }),(TEST_REVIEW.kid.clone(), Issuer{
            count: 1_i32,
            neutrality: None,
        })])),
        subjects: Some(HashMap::from([(
            TEST_REVIEW_SECOND.payload.sub.clone(),
            Subject{
                confirmed_count: 0_i32,
                count: 1_i32,
                opinion_count: 1_i32,
                positive_count: 1_i32,
                quality: None,
                sub: TEST_REVIEW_SECOND.payload.sub.clone()
            }
        ),(TEST_REVIEW.payload.sub.clone(),Subject{
            confirmed_count: 0_i32,
            count: 1_i32,
            opinion_count: 1_i32,
            positive_count: 1_i32,
            quality: None,
            sub: TEST_REVIEW.payload.sub.clone(),
        })])),
    };

   pub static ref TEST_BATCH_RETURN_PEMS: BatchReturn = BatchReturn{
        issuers: Some(HashMap::from([(TEST_REVIEW_SECOND.kid.clone(), Issuer{
            count: 1_i32,
            neutrality: None
        }),(TEST_REVIEW.kid.clone(), Issuer{
            count: 1_i32,
            neutrality: None,
        })])),
        subjects: None,
    };

    pub static ref TEST_BATCH_RETURN_SUBS: BatchReturn = BatchReturn{
        issuers: None,
        subjects: Some(HashMap::from([(TEST_REVIEW.payload.sub.clone(),Subject{
            confirmed_count: 0_i32,
            count: 1_i32,
            opinion_count: 1_i32,
            positive_count: 1_i32,
            quality: None,
            sub: TEST_REVIEW.payload.sub.clone(),
        }),(
            TEST_REVIEW_SECOND.payload.sub.clone(),
            Subject{
                confirmed_count: 0_i32,
                count: 1_i32,
                opinion_count: 1_i32,
                positive_count: 1_i32,
                quality: None,
                sub: TEST_REVIEW_SECOND.payload.sub.clone()
            }
        )])),
    };
}

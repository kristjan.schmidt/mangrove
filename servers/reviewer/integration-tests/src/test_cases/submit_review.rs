// ########################
// Test case Submit Review
// ########################

use super::{
    TestSequenceResult, CONFIGURATION, TEST_REVIEW, TEST_REVIEW_SECOND, TEST_REVIEW_THIRD,
};
use crate::apis::default_api::submit_jwt_review_put;
use anyhow::{anyhow, Result as AnyhowResult};

// Test 1 Submit review
// Success
pub async fn test_submit_review_success() -> AnyhowResult<TestSequenceResult> {
    let token = TEST_REVIEW.jwt.as_str();
    submit_jwt_review_put(&CONFIGURATION, token)
        .await
        .map(TestSequenceResult::Boolean)
        .map_err(|err| anyhow!(err))
}

// Test 2 Submit second review
pub async fn test_submit_second_review_success() -> AnyhowResult<TestSequenceResult> {
    let token = TEST_REVIEW_SECOND.jwt.as_str();
    submit_jwt_review_put(&CONFIGURATION, token)
        .await
        .map(TestSequenceResult::Boolean)
        .map_err(|err| anyhow!(err))
}

// Test 3 Submit third review
pub async fn test_submit_third_review_success() -> AnyhowResult<TestSequenceResult> {
    let token = TEST_REVIEW_THIRD.jwt.as_str();
    submit_jwt_review_put(&CONFIGURATION, token)
        .await
        .map(TestSequenceResult::Boolean)
        .map_err(|err| anyhow!(err))
}

// Test 4 Submit the same review twice
// Fail Ok
pub async fn test_submit_review_duplication() -> AnyhowResult<TestSequenceResult> {
    let token = TEST_REVIEW.jwt.as_str();
    match submit_jwt_review_put(&CONFIGURATION, token).await {
        Ok(boolean) => Err(anyhow!(boolean)),
        Err(err) => Ok(TestSequenceResult::SubmitJwtReviewPutError(err.to_string())),
    }
}

// Test 5 Submit invalid review without kid and jwk
// Fail Ok
pub async fn test_submit_invalid_review() -> AnyhowResult<TestSequenceResult> {
    let invalid_token: &str = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE3MDc3Mzk2MjcsInN1YiI6Imh0dHBzOi8vZXhhbXBsZS5jb20iLCJyYXRpbmciOjkwLCJvcGluaW9uIjoiR3JlYXQgdGVzdGluZyBsaW5rIGV4YW1wbGUuIiwibWV0YWRhdGEiOnsibmlja25hbWUiOiJUZXN0Tmlja05hbWUiLCJjbGllbnRfaWQiOiJodHRwczovL2V4YW1wbGUuY29tIn19.WChEt_1tpZ9mVAiL0StuCdx6t4ibJk7cpUrR4xX5adhZResjVnNLwvJaryF2bwkELOXinSS0GqYNedz8m2tyFA";
    match submit_jwt_review_put(&CONFIGURATION, invalid_token).await {
        Ok(boolean) => Err(anyhow!(boolean)),
        Err(err) => Ok(TestSequenceResult::SubmitJwtReviewPutError(err.to_string())),
    }
}

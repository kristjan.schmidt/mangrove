CREATE TABLE reviewers (
    pem text NOT NULL PRIMARY KEY,
    neutrality float4 NOT NULL
);

CREATE TABLE subjects (
    sub text NOT NULL PRIMARY KEY,
    quality int2 NOT NULL
);

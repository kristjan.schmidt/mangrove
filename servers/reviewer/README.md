# Mangrove Review Server

See [the API docs here](https://docs.mangrove.reviews).

## Run Postgres database

```
docker-compose -d up
```

Environment variables:

### Postgres

- POSTGRES_USER (default 'postgres')
- POSTGRES_PASSWORD (default 'postgres')

### PgAdmin

- PGADMIN_SOFTWARE_VERSION_TAG (default 'latest')
- PGADMIN_DEFAULT_EMAIL (default 'pgadmin@pgadmin.org')
- PGADMIN_DEFAULT_PASSWORD (default 'pgadmin')
- PGADMIN_LISTEN_PORT (default '8080')

## Build and run the server.

```
cargo run --bin reviewer
```

Environment variables:

- PORT_NUMER (default 8000)
- DATABASE_URL

Example: DATABASE_URL=postgresql://postgres:postgres@localhost:5432/

## Api doc generation
Use the following command to autogenerate reviewer api doc based on code under endpoints.

```
cd servers/reviewer
cargo run --bin api_doc
```

### Env variables:
- OPENAPI_FILE_PATH - specify the openapi file path to save to (default: "./docs/mangrove_openapi.yml")

## Common SQL operations

Select all orphaned reactions.
```
select * from public.reviews
where scheme='urn:maresi' and sub not in (select concat('urn:maresi:', signature) from public.reviews)
```
